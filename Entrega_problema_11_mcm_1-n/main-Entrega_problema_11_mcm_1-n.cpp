/*Este programa recibe como entrada por teclado un numero y devuelve
 * cual es el minimo comun multiplo de todos los numeros entre 1 y el numero ingresado
 * el programa requiere que usted ingrese un numero entero positivo
 * si usted ingresa un numero entero positivo de menos de dos cifras el programa tardara entre
 * 5 y 10 segundos en dar una respuesta, para numeros de mas cifras puede tardar considerablemente
*/
#include <iostream>

using namespace std;

int main()
{
    int i,numero,nuevo,primermultiplo,residuo,contador=0;
    cout << "ingrese un numero: ";
    cin>>numero;
    nuevo=numero-1;
    primermultiplo=numero*nuevo;    //producto de el numero ingresado y el mismo disminuido en 1
    for(i=1;i<=numero;i++){
        residuo=primermultiplo%i;  //obtener el residuo de todos los numeros entre 1 y el numero ingresado
        if (residuo==0)
            contador+=1;        //AUMENTAR CONTADOR CADA QUE ENCUENTRA UA DIVISION EXACTA
        else{
        primermultiplo++;   //DE LO CONTRARIO AUMENTA EL PRODUCTO OBTENIDO EN LA LINEA 17 EN 1
        contador=0;         //Y REINICIA CONTADOR
        i=1;}               //REINICA i

}

cout<<"El minimo comun multiplo es: "<<primermultiplo<<endl;

    return 0;
}
