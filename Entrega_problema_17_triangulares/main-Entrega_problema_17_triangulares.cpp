/*Este programa recibe como entrada un numero k y calcula cual es en menor numero
 * triangular que tiene un numero de divisores mayor a k
 * para el funcionamien se requiere que se ingrese un numero entero positivo
 *si usted ingresa un numero k entero positivo el programa mostrara
 *  en pantalla cual es el menor numero triangular con mas de k divisores
 * ademas dira cuantos divisores tiene ese numero exactamente.
 * para numeros de una cifra el programa se ejecuta casi de manera instantanea, peo
 * a medida que las cifras de k aumenten tardara mas en dar una respuesta
*/
#include <iostream>
using namespace std;

int main()
{
    int numero,j,contador=0,i=1,divisor,triangular=0;

    cout<<"ingrese un numero k: ";
    cin>>numero;

    for (i=1;contador<=numero;i++){
        triangular=(i*(i+1))/2; //crea un numero triangular cada que i aumente en uno
        if(contador<=numero){ //si el contador no sobrepasa k
            contador=0;}//contador se reinicia
        for (j=1;j<=triangular;j++){
            divisor=(triangular%j); //busca todos los divisores del numero triangular generado
            if (divisor==0){  //cuando encuentra un divisor
                contador+=1;}   //aumenta el contador en uno
}
    }
cout<<"El numero es: "<<triangular<<" y tiene "<<contador<<" divisores. "<<endl;
    return 0;
}
