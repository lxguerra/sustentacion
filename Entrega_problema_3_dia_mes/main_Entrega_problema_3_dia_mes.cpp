/* Este programa pide como primera entrada un mes,
 * luego un día y determina si la combincación ingresada
 * es valida como fecha, en caso de que el mes ingresado sea febrero
 * y el dia 29 dira que es valida si el año es bisiesto
 * El programa requiere dos entradas numericas por teclado, la primera para mes y la segunda para dia.
 * Si se ingresan valores para mes entre 1 y 12 y valores para dia entre 1 y 31, el programa
 * puede asegurar si el mes que ingreso contiene dias menores o iguales a los ingresados
 * y le dira que es una fecha valida, de lo contrario le dira que es una fecha invalida
*/

#include <iostream>

using namespace std;
int main()
{

        int dia, mes;
        cout << "Ingrese el numero del mes: ";
        cin >>mes;
        if (mes >=13 || mes <=0)
        {
            cout <<mes<<" es un mes invalido."<<endl;
        }
        else
        {
            cout << "Ingrese el numero del dia: ";
            cin >>dia;


        if ( mes >= 1 && mes <= 12 ) // si el mes enta en el rango de uno a doce
        {
            switch ( mes )
            {
                case  1 ://estos son
                case  3 ://los casos
                case  5 ://para los
                case  7 ://meses
                case  8 ://que tienen
                case 10 ://31 dias
                case 12 : if ( dia >= 1 && dia <= 31 )// si el dia esta entre 1 y 31
                            cout <<dia<<"/"<<mes<<" es una fecha valida"<<endl;//imprime los mensajes de acierto
                          else                                                    //o
                            cout <<dia<<"/"<<mes<<" es una fecha invalida"<<endl;//de error
                          break;

                case  4 ://estos
                case  6 ://para meses
                case  9 ://de 30 dias
                case 11 : if ( dia >= 1 && dia <= 30 )// de nuevo se se cumple la condicion imprime los mensajes
                            cout <<dia<<"/"<<mes<<" es una fecha valida"<<endl;
                          else
                            cout <<dia<<"/"<<mes<<" es una fecha invalida"<<endl;
                          break;

                /* en este ultimo caso se evalua especificamente por el dia 29 de febrero
                 *  ya que este mes es el unico que tiene 28 dias solamente
                 * y cada cuatro años segun el calendario gregoriano se debe
                 * agregar un dia mas a este mes, lo que crea el año conocido como bisiesto
                 * tambien se evaluan los otros dias del mes en caso que el
                 * dia ingresado no sea 29
                */
                case  2 : if( mes == 2 && dia >= 1 && dia <=29 )
                          if ( dia == 29 )
                            cout <<dia<<"/"<<mes<<" es valida en bisiesto"<<endl;
                          else if (mes==2 && dia >= 1 && dia <=28)
                            cout <<dia<<"/"<<mes<<" es una fecha valida"<<endl;
                          else
                            cout <<dia<<"/"<<mes<<" es una fecha invalida"<<endl;
            }
        }
        else
            cout <<dia<<"/"<<mes<<" es una fecha invalida"<<endl;

}
        return 0;
}

