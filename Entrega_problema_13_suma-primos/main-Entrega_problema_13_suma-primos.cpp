/*Este programa calcula la suma de todos los numeros primos existentes
 * entre uno y el numero ingresado, ademas mostrara en pantalla
 * cada numero primo encontrado.
 * El programa requiere la entrada por teclado de un numero.
 * Si se ingresa un numero entero positivo se obtendra la suma de
 * todos los numeros primos existentes entre uno y ese numero
*/
#include <iostream>

using namespace std;

int main()
{
    int primo,dividir,divisores,suma=0,numero,contador=0;
    cout << "Ingrese un numero: ";
    cin>>numero;
    for ( primo=2;primo<numero;primo++){//lista de numeros entre 2 y numero ingresado
        for ( divisores=1;divisores<=primo;divisores++){//lista de divisores de cada numero de la lista
            dividir=primo%divisores;//este bloque
            if(dividir==0)          //busca los
                contador=contador+1;//divisores del numero en la lista
                            }
                if (contador<=2){//si tiene dos o menos
                suma=suma+primo;//los suma
                cout<<primo<<endl;}
                contador=0;//reiniciar contador para cada nuevo numero de lista


                            }
    cout<<"El resultado de la suma es: "<<suma<<endl;
    return 0;
}
