/* ESTE PROBLMA RECIBE COMO ENTRADA POR TECLADO UN NUMERO ENTERO POSITIVO
 * LO SEPARA EN DIGITOS Y LUEGO ELEVA CADA DIGITO A SI MISMO
 * POR ULTIMO ENTREGA LA SUMA DE LAS POTENCIAS DE TODOS LOS DIGITOS
 *

*/

#include <iostream>
#include <math.h>    //INCLUIR LA LIBRERIA MATH
using namespace std;

int main()
{
    int numero,divisor=0,aux,resultado=0;
   cout<<"ingrese un numero: ";
    cin>>numero;
    aux=numero;
    if(numero==0){
    cout<<"la suma es: "<<aux<<endl;
    }
    else{
        while (numero>0){
        divisor=numero%10;//OBTENER EL DIGITO CORRESPONDIENTE A LAS UNIDADES SUCESIVAMENTE
        resultado=resultado+(pow(divisor,divisor));//ELEVAR EL NUMERO OBTENIDO A SI MISMO
        numero=numero/10;
    }
     cout<<"la suma es: "<<resultado<<endl;
        }

    return 0;
}
